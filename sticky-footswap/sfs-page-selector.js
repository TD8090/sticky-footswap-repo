jQuery(document).ready(function($) {
    $('.customize-control-pages input[type="checkbox"]').change(function() {
        var vals = $('.customize-control-pages input:checked').map(function() {
            return $(this).val();
        }).get().join(',');
        $('.customize-control-pages input[type="text"]').val(vals).trigger('change');
    });
});