<?php
/**
 * Sticky Footswap By Todd Durica.
 *
 * Plugin Name:		Sticky Footswap
 * Plugin URI:		http://td1.me
 * Description:		Configure sticky header and sticky footer within any wordpress theme.
 * Author:			Todd Durica
 * Author URI:		http://td1.me
 */


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once( plugin_dir_path( __FILE__ ) . 'sticky-footswap-class.php' );
require_once( plugin_dir_path( __FILE__ ) . 'sticky-footswap-settings.php' );

add_action( 'plugins_loaded', array( 'Sticky_Footswap', 'tdsfsfn_get_instance' ) );