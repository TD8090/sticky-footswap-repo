<?php
/**
 * Define the Sticky_Footswap class which is called at the very end of the main plugin file (sticky-footswap.php) with tdsfsfn_get_instance.
 */
class Sticky_Footswap {
	/**	 * Plugin version, used for cache-busting of style and script file references.	 * @var     string	 */
	const VERSION = '1.0.0';
	/**	 * Unique identifier for your plugin.
	 * The variable name is used as the text domain when internationalizing strings of text.
	 * Its value should match the Text Domain file header in the main plugin file.
	 * @var      string	 */
	protected $plugin_slug = 'td-stickyfootswap';
	/**	 * Instance of this class.	 * @var      object	 */
	protected static $instance = null;
	/**	 * Slug of the plugin screen.	 * @var      string	 */
	protected $plugin_screen_hook_suffix = null;
	/**	 * Initialize the plugin by setting localization, filters, and administration functions.	 */
	private function __construct() {
		// Add an action link pointing to the options page.
		$plugin_mainpath = plugin_basename( plugin_dir_path( __FILE__ ) . 'sticky-header.php' );
		add_filter( 'plugin_action_links_' . $plugin_mainpath, array( $this, 'tdsfsfn_add_action_links' ) );
		// Load public-facing style sheet and JavaScript.
		add_action( 'wp_enqueue_scripts', array( $this, 'tdsfsfn_enqueue_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'tdsfsfn_enqueue_scripts' ) );
		// Define custom functionality.
		add_action( 'wp_footer', array( $this, 'tdsfsfn_htmlgen' ) ); // Display Sticky Header gen-html.php PHP 'view' in the wp_footer which is defined below
		add_action( 'wp_head', array( $this, 'tdsfsfn_cssgen' ) ); // Sticky Header generated CSS
	}
	/**	 * Return an instance of this class.	 * @return    object    A single instance of this class.	 */
	public static function tdsfsfn_get_instance() {
		if ( null == self::$instance ) {		// If the single instance hasn't been set, set it now.
			self::$instance = new self;
		}		return self::$instance;	}
	/**	 * Register and enqueue public-facing style sheet.	 */
	public function tdsfsfn_enqueue_styles() {
		wp_enqueue_style( $this->plugin_slug . '-plugin-styles', plugins_url( 'css/public.css', __FILE__ ), array(), self::VERSION );
	}
	/**	 * Register and enqueues public-facing JavaScript files.	 */
	public function tdsfsfn_enqueue_scripts() {
		wp_enqueue_script( $this->plugin_slug . '-plugin-script', plugins_url( 'js/public.js', __FILE__ ), array( 'jquery' ), self::VERSION );
		// Send plugin settings to JS file.
		$plugin_settings = get_option( 'tdsfs' );

		/*php debug helper*/function debug_to_console( $data ) { if ( is_array( $data ) ) $output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";	else  $output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>"; echo $output;		}
		debug_to_console( $plugin_settings );

		$to_js_params   = array(
			'show_at'          		=> $plugin_settings['show_at'],
			'wn_threshold'      	=> $plugin_settings['wn_threshold'],
			'disable_if_narrower' 	=> $plugin_settings['disable_if_narrower'],
			'MCHECK2' 				=> $plugin_settings['MCHECK2'],
			'mselect_pages' 		=> $plugin_settings['mselect_pages'],
			'mselect_posts' 		=> $plugin_settings['mselect_posts'],
			'mcheck_pages' 			=> $plugin_settings['mcheck_pages'],
			'mcheck_posts' 			=> $plugin_settings['mcheck_posts'],
//			'mcheck_cats' 			=> $plugin_settings['mcheck_cats'],
		);
		wp_localize_script( $this->plugin_slug . '-plugin-script', 'StickyFootswapParams', $to_js_params );   // wp_localize_script packages StickyFootswapParams
	}
	/**	 * Add settings action link to Customize page.	 */
	public function tdsfsfn_add_action_links( $links ) {
		return array_merge(
			array(
				'settings' => '<a href="' . admin_url( 'customize.php' ) . '">' . __( 'Settings', $this->plugin_slug ) . '</a>'
			),			$links		);	}
	/**	 * PHP output (this is the view that is flagged to be inserted in the wp_foot in the call above)	 */
	public function tdsfsfn_htmlgen() {
		include_once( 'views/gen-html.php' );
	}
	public function tdsfsfn_cssgen() {
		include_once( 'views/gen-css.php' );
	}
}