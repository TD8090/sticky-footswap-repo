=== Sticky Footswap by Todd Durica ===
Contributors:
Tags: sticky footer, footer, swap footers, fixed footer
Requires at least: 3.5
Tested up to: 4.0.1
Stable tag: 1.0
License: GPLv2+
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This readme is still a template.


Sticky Footswap by Todd Durica allows you to add sticky footer to any WordPress theme that replaces your main footer.

== Description ==

For support or suggestions please contact me at td8090@gmail.com.

The Sticky Footswap WordPress plugin makes your footer visible when you scroll, which keeps . It's a great feature that allows people to browse faster and consume more of your content.

Sticky Footswap by Todd Durica is part of your brand building, improves user experience by saving people time and makes your WordPress website more aesthetically pleasing.

Your branding (logo) is something you want visitors to subconsciously keep in mind while browsing your website and navigation is one of the most important aspects of every website. Make them visible at all times.

= Settings =

Sticky Footswap by Todd Durica has just few options, which you can find in your Theme Customizer. This makes it *incredibly easy to configure and use*.

* **Menu** - You need to create at least one navigation menu before you can add it to Sticky Footswap. The menu in Sticky Footswap **shows only first level items**, so no submenu items will be shown.
* **Logo** - You can upload your sticky header logo by clicking on "Upload new" > "Select a file". Ideally, this **logo should be 30px** tall or it will be resized. If a logo image is not uploaded, site title will be used.
* **Background color** - You can choose your custom sticky header background color.
* **Text color** - Choose your sticky header text color so it provides enough contrast with regards to the background color.
* **Sticky Footswap max width** - This option allows you to match interior Sticky Footswap width to your content area width.
* **Make visible when scrolled to** - This is the distance from the top of the page at which sticky header will show, in pixels. 
* **Hide if screen is narrower than** - Sticky header will not be shown on screens narrower than this value, in pixels. When people use mobile devices, they're used to vertical scrolling so it's not necessary to have sticky headers.

== Installation ==

1. Download the plugin from this page and extract it
2. Copy the sticky-header folder to the "/wp-content/plugins/" directory 
3. Activate the plugin through the "Plugins" menu in WordPress dashboard
4. Setup your sticky header from Appearance > Customize


== Frequently Asked Questions ==

= Where's the plugin's settings page =
It's located in Appearance > Customize > Sticky Footswap by Todd Durica.

== Screenshots ==

1. Sticky header in action
2. Sticky Footswap Customizer options

== Changelog ==

= 1.2 =
* Fixes a PHP notice.

= 1.1 =
* Added Sticky Footswap max interior width.

= 1.0.1 =
* Fixed header’s z-index.

= 1.0 =
* First version.