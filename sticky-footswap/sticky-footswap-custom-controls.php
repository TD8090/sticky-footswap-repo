<?php

/**
 * Register 2 custom controls for number input and multicheck.
 * See https://github.com/gambitph/Titan-Framework/blob/v1.4.2/class-option-multicheck.php for more details.
 */
if (class_exists('WP_Customize_Control')) {

	// Define Number custom control
	class Sticky_Footswap_Number_Control extends WP_Customize_Control
	{
		public $type = 'number';

		public function render_content()
		{ ?>
			<label>
				<span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
				<input class="small-text" type="number"
					   value="<?php echo esc_attr($this->value()); ?>" <?php $this->link(); ?> />
			</label>
		<?php }
	}
	// Define Multicheck custom control
	class Sticky_Footswap_Multicheck_Control extends WP_Customize_Control
	{
		public $subtitle = '';
		public $type = 'mcheck';
		private static $firstLoad = true;

		public function render_content() {

			// the saved value is an array. convert it to csv
			if (is_array($this->value())) {
				$savedValueCSV = implode(',', $this->value());
				$values = $this->value();
			} else {
				$savedValueCSV = $this->value();
				$values = explode(',', $this->value());
			}

			if (self::$firstLoad) {
				self::$firstLoad = false;

				?>
				<script>
					jQuery(document).ready(function ($) {
						"use strict";

						$('input.sfs-multicheck').change(function (event) {
							event.preventDefault();
							var csv = '';
/* FIX NEEDED (ffox)*/
							$(this).parents('li:eq(0)').find('input[type=checkbox]').each(function () {
								if ($(this).is(':checked')) {
									csv += $(this).attr('value') + ',';
								}
							});
							// REPLACE (Transact-SQL),http://msdn.microsoft.com/en-us/library/ms186862.aspx
							// replace( string_expression , string_pattern , string_replacement )
							// replace b in a with c
							csv = csv.replace(/,+$/, "");

							$(this).parents('li:eq(0)').find('input[type=hidden]').val(csv)
								// we need to trigger the field afterwards to enable the save button
								.trigger('change');
							return true;
						});
					});
				</script>
			<?php
			} ?>
			<label class='sfs-multicheck-container'>
				<span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
				<?php if ('' != $this->subtitle) : ?>
				<div class="customizer-subtitle"><?php echo $this->subtitle; ?></div>
				<?php endif; ?>


				<?php
				foreach ($this->choices as $value => $label) {
					printf('<label for="%s"><input class="sfs-multicheck" id="%s" type="checkbox" value="%s" %s/> %s</label><br>',
						$this->id . $value,
						$this->id . $value,
						esc_attr($value),
						checked(in_array($value, $values), true, false),
						$label
					);
				}
				?>
				<input type="hidden" value="<?php echo esc_attr($savedValueCSV); ?>" <?php $this->link(); ?> />
			</label>
		<?php

		}
	}

	// MCHECK2 custom control
	class Customize_Pages_Control extends WP_Customize_Control {
		public $type = 'mck2';
		public function enqueue() { // Enqueue script
			wp_register_script( 'sfs-page-selector', get_stylesheet_directory_uri() . '/sfs-page-selector.js', array(), '', false );
			wp_enqueue_script( 'sfs-page-selector' );
		}
		public function render_content() {
			$pages = get_pages(); ?>
			<span class="customize-control-title">
				<?php echo esc_html( $this->label ); ?>
			</span>
			<input type="hidden" value="" <?php $this->link()?>/>
			<?php // Field to hold final values
			foreach ($pages as $page) { // Loop through pages and add checkboxes
				?>
				<label>
					<input type="checkbox" value="<?php echo $page->ID ?>">
					<?php echo $page->post_title; ?>
				</label> <?php
			}
		}
	}


/*vvv BEST WORKING vvv*/


	class Sticky_Footswap_Multiple_Select_Control extends WP_Customize_Control {

		/**
		 * The type of customize control being rendered.
		 */
		public $type = 'm-select';

		/**
		 * Displays the multiple select on the customize screen.
		 */
		public function render_content() {

			if ( empty( $this->choices ) )
				return;
			?>
			<label>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<select <?php $this->link(); ?> multiple="multiple" style="height: 100%;">
					<?php
					foreach ( $this->choices as $value => $label ) {
						$selected = ( in_array( $value, $this->value() ) ) ? selected( 1, 1, false ) : '';
						echo '<option value="' . esc_attr( $value ) . '"' . $selected . '>' . $label . '</option>';
					}
					?>
				</select>
			</label>
		<?php }
	}
}