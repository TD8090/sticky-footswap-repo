/**
 * Created by Brak on 12/17/2014.
 */

/* ---------------------------------------------------------------------------
 * Example(s) from cake theme
 * --------------------------------------------------------------------------- */
var mfn_header_height = $('#Top_bar').innerHeight();
function mfn_sticky(){
    if( $('body').hasClass('sticky-header') ){
        var start_y = mfn_header_height;
        var window_y = $(window).scrollTop();

        if( window_y > start_y ){ 													// pass the threshold
            if( ! ($('#Top_bar').hasClass('is-sticky'))) {							// execute only if target element doesn't have the class is-sticky
                $('.header_placeholder').css('margin-top', mfn_header_height);
                $('#Top_bar')
                    .addClass('is-sticky')
                    .css('top',-60)
                    .animate({
                        'top': $('#wpadminbar').innerHeight()
                    },300);
            }
        }
        else {																		// return across threshold
            if($('#Top_bar').hasClass('is-sticky')) {								// if it does have is-sticky,
                $('.header_placeholder').css('margin-top',0);						// set margin top to 0 on our invis replacement element .
                $('#Top_bar')														// top bar: remove is-sticky, apply css rule top to 0
                    .removeClass('is-sticky')
                    .css('top', 0);
            }
        }
    }
}
/* --------------------------------------------------------------------------------------------------------------------------
 * $(document).ready
 * ----------------------------------------------------------------------------------------------------------------------- */

/* back to top animation
 * */
$('#back_to_top').click(function(){
    $('body,html').animate({
        scrollTop: 0
    }, 500);
    return false;
});

/* Anchor Fix + Smooth scroll
 * */
var hash = window.location.hash;
if( hash && $(hash).length ){
    var stickyH = $('#Top_bar.is-sticky').innerHeight();
    var tabsHeaderH = $(hash).siblings('.ui-tabs-nav').innerHeight();

    $('html, body').animate({
        scrollTop: $(hash).offset().top - 0 - stickyH - tabsHeaderH
    }, 500);
}

$('#menu li.scroll > a').click(function(){
    var url = $(this).attr('href');
    var hash = '#' + url.split('#')[1];
    var stickyH = $('#Top_bar.is-sticky').innerHeight();
    var tabsHeaderH = $(hash).siblings('.ui-tabs-nav').innerHeight();

    if( hash ){
        $('html, body').animate({
            scrollTop: $(hash).offset().top - 0 - stickyH - tabsHeaderH
        }, 500);
    }
});
/* --------------------------------------------------------------------------------------------------------------------------
 * $(window).scroll
 * ----------------------------------------------------------------------------------------------------------------------- */
$(window).scroll(function(){   // "on every scroll event, execute this function"
    mfn_sticky();
});
/* --------------------------------------------------------------------------------------------------------------------------
 * /END cake theme example
 * ----------------------------------------------------------------------------------------------------------------------- */