<?php
/** * Handles Customizer admin options. */
add_action( 'customize_register', 'tdsfsfn_customize_register' );
/** * Registers all Customizer options. */
function tdsfsfn_customize_register( $wp_customize ) {

	$author_plugin_slug = 'td-stickyfootswap';

	// Define microfoot custom control - sticky replacement footer for short pages
	require_once( plugin_dir_path( __FILE__ ) . 'sticky-footswap-custom-controls.php' );

//LOGIC: package arrays by page, category, tag, and post for multicheck display
// http://codex.wordpress.org/Function_Reference/get_pages
/*	function get_categories_array() {
		$teh_cats = get_categories();
		$results = [];

		$count = count($teh_cats);
		for ($i=0; $i < $count; $i++) {
			if (isset($teh_cats[$i]))
				$results[$teh_cats[$i]->slug] = $teh_cats[$i]->name;
			else
				$count++;
		}
		return $results;
	}*/
	$pages = get_pages();
	if ( $pages ) {
		$prop = 'post_title';
		$pages = wp_list_pluck( $pages, $prop );
	}
	$posts = get_posts();
	if ( $posts ) {
		$prop = 'post_title';
		$posts = wp_list_pluck( $posts, $prop );
	}

/*REGISTER THE SECTION NAME*/
	$wp_customize->add_section(
		'tdsfs', array(
			'title'	   => __( 'Sticky Footswap Settings', $author_plugin_slug ),
			'priority' => 1
	));


	// NEW MCHECK2 CONTROL
	$wp_customize->add_setting( 'tdsfs[MCHECK2]' , array(
		'capability'    => 'edit_theme_options',
		'sanitize_callback' => 'absint',
		'type' => 'mck2',
	) );
	$wp_customize->add_control( new Customize_Pages_Control ( $wp_customize,
		'tdsfs[MCHECK2]', array(
			'label' => __('Add pages to js param', $author_plugin_slug),
			'section' => 'tdsfs',
			'settings' => 'tdsfs[MCHECK2]',
			'priority' => 1

		) ) );


//IMPORTED876 Jayj_Customize_Control_Multiple_Select
	$wp_customize->add_setting( 'tdsfs[mselect_pages]', array(
		'default'	=> array(),
		'sanitize_callback' => 'absint',
		'type'			=> 'm-select',
		'capability'	=> 'edit_theme_options',		));
	$wp_customize->add_control(	new Sticky_Footswap_Multiple_Select_Control(	$wp_customize,
			'tdsfs[mselect_pages]',
			array(
				'label'    => __('Page mselect', $author_plugin_slug),
				'section'  => 'tdsfs',
				'settings' => 'tdsfs[mselect_pages]',
				'choices'  => $pages,
				'priority' => 2
			)));
	$wp_customize->add_setting( 'tdsfs[mselect_posts]', array(
		'default'	=> array(),
		'sanitize_callback' => 'absint',
		'capability'	=> 'edit_theme_options',
		'type'			=> 'm-select',
	));
	$wp_customize->add_control(	new Sticky_Footswap_Multiple_Select_Control(	$wp_customize,
			'tdsfs[mselect_posts]',
			array(
				'label'    => __('Post mselect', $author_plugin_slug),
				'section'  => 'tdsfs',
				'settings' => 'tdsfs[mselect_posts]',
				'choices'  => $posts,
				'priority' => 3
			)));
	// Multicheck PAGES
	$wp_customize->add_setting(	'tdsfs[mcheck_pages]', array(
		'sanitize_callback' => 'absint',
		'capability'	=> 'edit_theme_options',	));
	$wp_customize->add_control( new Sticky_Footswap_Multicheck_Control(	$wp_customize,
		'tdsfs[mcheck_pages]', array(
			'label'       => __('Page Select', $author_plugin_slug),
			'section'     => 'tdsfs',
			'settings'    => 'tdsfs[mcheck_pages]',
			'choices'     => $pages,
			'type'		  => 'mcheck',
			'subtitle'    => 'Any Page URLs to swap footer on',
			'priority'    => 6
		)	));

/*	// Multicheck CATEGORIES
	$wp_customize->add_setting(	'tdsfs[mcheck_cats]', array(
		'sanitize_callback' => 'absint',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',	));
	$wp_customize->add_control( new Sticky_Footswap_Multicheck_Control(	$wp_customize,
		'tdsfs[mcheck_cats]', array(
			'label'       => __('Category Select', $author_plugin_slug),
			'section'     => 'tdsfs',
			'settings'    => 'tdsfs[mcheck_cats]',
			'choices'     => get_categories_array(),
			'subtitle'    => 'Any Category URLs to swap footer on',
			'priority'    => 2
		)	));*/

	// Multicheck POSTS
	$wp_customize->add_setting(	'tdsfs[mcheck_posts]', array(
		'sanitize_callback' => 'absint',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',	));
	$wp_customize->add_control( new Sticky_Footswap_Multicheck_Control(	$wp_customize,
		'tdsfs[mcheck_posts]', array(
			'label'       => __('Post Select', $author_plugin_slug),
			'section'     => 'tdsfs',
			'settings'    => 'tdsfs[mcheck_posts]',
			'choices'     => $posts,
			'subtitle'    => 'Any Post URLs to swap footer on',
			'priority'    => 7
		)	));


// // // // // // // // // // // // // // // // // // // // // // // //

	// [menu]  select from available WP menus
	$menus = wp_get_nav_menus();
	if ( $menus ) :
		$choices = array( 0 => __( '&mdash; Select a menu &mdash;' ) );
		foreach ( $menus as $menu ) :
			$choices[ $menu->term_id ] = wp_html_excerpt( $menu->name, 40, '&hellip;' );
		endforeach;

		$wp_customize->add_setting(
			'tdsfs[menu]',
			array(	'sanitize_callback' => 'absint','theme_supports'=> 'menus',
				'type'				=> 'option','capability'	=> 'edit_theme_options',
			)	);
		$wp_customize->add_control(
			'tdsfs[menu]',
			array(
				'label'   	=> __( 'old menu', $author_plugin_slug ),
				'section' 	=> 'tdsfs',
				'type'    	=> 'select',
				'choices' 	=> $choices,
				'priority'	=> 15
			)
		);
	endif;



	// [show_at] Sticky Header show at
	$wp_customize->add_setting(
		'tdsfs[show_at]',	array(
			'default'=> '100',	 'sanitize_callback' => 'wp_filter_nohtml_kses',
			'type'	 => 'option','capability'		 => 'edit_theme_options',	));
	$wp_customize->add_control(
		new Sticky_Footswap_Number_Control(
			$wp_customize,
			'tdsfs[show_at]',
			array(
				'label'		=> __( 'Show at Scroll distance (px dist)', $author_plugin_slug ),
				'section'	=> 'tdsfs',
				'settings'	=> 'tdsfs[show_at]',
				'priority'	=> 5
			)
		)
	);






	// [wn_threshold] Sticky Header Wide-Narrow Threshold
	$wp_customize->add_setting(
		'tdsfs[wn_threshold]',
		array(
			'default'			=> '600',
			'sanitize_callback' => 'wp_filter_nohtml_kses',
			'type'				=> 'option',
			'capability'		=> 'edit_theme_options',
		)
	);
	$wp_customize->add_control(
		new Sticky_Footswap_Number_Control(
			$wp_customize,
			'tdsfs[wn_threshold]',
			array(
				'label'		=> __( 'Wide-Narrow Threshold (px width)', $author_plugin_slug ),
				'section'	=> 'tdsfs',
				'settings'	=> 'tdsfs[wn_threshold]',
				'priority'	=> 10
			)
		)
	);


	// [wide_state_logo] Upload Sticky Header logo for wide-state
	$wp_customize->add_setting(
		'tdsfs[wide_state_logo]',
		array('default'	=> '',		'sanitize_callback' => 'esc_url_raw',
			  'type'	=> 'option','capability'=> 'edit_theme_options',		)	);
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'tdsfs[wide_state_logo]',
			array(
				'label'		=> __( 'Wide-State Logo Image', $author_plugin_slug ),
				'section'	=> 'tdsfs',
				'settings'	=> 'tdsfs[wide_state_logo]',
				'priority'	=> 20
	)));
	// [wide_state_logo] Upload Sticky Header logo for wide-state
	$wp_customize->add_setting(
		'tdsfs[background_image]',
		array('default'	=> '',		'sanitize_callback' => 'esc_url_raw',
			  'type'	=> 'option','capability'=> 'edit_theme_options',		)	);
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'tdsfs[background_image]',
			array(
				'label'		=> __( 'Background Image (repeat x)', $author_plugin_slug ),
				'section'	=> 'tdsfs',
				'settings'	=> 'tdsfs[background_image]',
				'priority'	=> 21
	)));


	// [narrow_state_expandicon] Header Narrow-state expand icon
	$wp_customize->add_setting(
		'tdsfs[narrow_state_expandicon]',
		array(
			'default'	=> '',		'sanitize_callback' => 'esc_url_raw',
			'type'		=> 'option','capability'		=> 'edit_theme_options',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'tdsfs[narrow_state_expandicon]',
			array(
				'label'		=> __( 'Narrow-State Expand-Icon (>25px)', $author_plugin_slug ),
				'section'	=> 'tdsfs',
				'settings'	=> 'tdsfs[narrow_state_expandicon]',
				'priority'	=> 30

			)
		)
	);

















//EVERYTHINGS OK HERE
	// [background_color] Sticky Header background color
	$wp_customize->add_setting(
		'tdsfs[background_color]',
		array(
			'default'			=> '#181818',
			'sanitize_callback' => 'wp_filter_nohtml_kses', // Used instead of HTMLPurifier
			'type'				=> 'option',
			'capability'		=> 'edit_theme_options',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'tdsfs[background_color]',
			array(
				'label'		=> __( 'Background color', $author_plugin_slug ),
				'section'	=> 'tdsfs',
				'settings'	=> 'tdsfs[background_color]',
				'priority'	=> 45
			)
		)
	);
//EVERYTHINGS OK HERE
	// [text_color] Sticky Header text color
	$wp_customize->add_setting(
		'tdsfs[text_color]',
		array(
			'default'			=> '#f9f9f9',
			'sanitize_callback' => 'wp_filter_nohtml_kses',
			'type'				=> 'option',
			'capability'		=> 'edit_theme_options',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'tdsfs[text_color]',
			array(
				'label'		=> __( 'Text color', $author_plugin_slug ),
				'section'	=> 'tdsfs',
				'settings'	=> 'tdsfs[text_color]',
				'priority'	=> 50
			)
		)
	);
	//TODO NARROW LOGO POSITION (LEFT OR CENTER) hmmmm....later
	// [inner_max_width] Sticky Header Logo/Menu max width
	$wp_customize->add_setting(
		'tdsfs[inner_max_width]',
		array(	'default' => '',  'sanitize_callback' => 'wp_filter_nohtml_kses',
				'type'	  => 'option','capability'	  => 'edit_theme_options',		));
	$wp_customize->add_control(
		new Sticky_Footswap_Number_Control(
			$wp_customize,
			'tdsfs[inner_max_width]',
			array(
				'label'	    => __( 'Left/Right container widest needed max width (px width) (Wide or unset setting pins apart left & right. Narrow setting forces stack effect)', $author_plugin_slug ),
				'section'	=> 'tdsfs',
				'settings'	=> 'tdsfs[inner_max_width]',
				'priority'	=> 55
			)
		)
	);
	$wp_customize->add_setting(
		'tdsfs[inner_medium_mxw_bpoint]',
		array(	'default' => '',	  'sanitize_callback' => 'wp_filter_nohtml_kses',
				'type'	  => 'option','capability'		  => 'edit_theme_options',		));
	$wp_customize->add_control(
		new Sticky_Footswap_Number_Control(
			$wp_customize,
			'tdsfs[inner_medium_mxw_bpoint]',
			array(
				'label'	    => __( 'UNTIL medium trigger at (px width breakpoint)(near above value)', $author_plugin_slug ),
				'section'	=> 'tdsfs',
				'settings'	=> 'tdsfs[inner_medium_mxw_bpoint]',
				'priority'	=> 56
			)
		)
	);
	$wp_customize->add_setting(
		'tdsfs[inner_medium_mxw]',
		array(	'default' => '',	  'sanitize_callback' => 'wp_filter_nohtml_kses',
				'type'	  => 'option','capability'		  => 'edit_theme_options',		));
	$wp_customize->add_control(
		new Sticky_Footswap_Number_Control(
			$wp_customize,
			'tdsfs[inner_medium_mxw]',
			array(
				'label'	    => __( 'Where (px max width) is needed...', $author_plugin_slug ),
				'section'	=> 'tdsfs',
				'settings'	=> 'tdsfs[inner_medium_mxw]',
				'priority'	=> 57
			)
		)
	);

		$wp_customize->add_setting(
		'tdsfs[inner_narrow_mxw_bpoint]',
		array(	'default' => '',	  'sanitize_callback' => 'wp_filter_nohtml_kses',
				'type'	  => 'option','capability'		  => 'edit_theme_options',		));
	$wp_customize->add_control(
		new Sticky_Footswap_Number_Control(
			$wp_customize,
			'tdsfs[inner_narrow_mxw_bpoint]',
			array(
				'label'	    => __( 'UNTIL narrow trigger at (px width breakpoint)(near above value)', $author_plugin_slug ),
				'section'	=> 'tdsfs',
				'settings'	=> 'tdsfs[inner_narrow_mxw_bpoint]',
				'priority'	=> 58
			)
		)
	);
	$wp_customize->add_setting(
		'tdsfs[inner_narrow_mxw]',
		array(	'default' => '',	  'sanitize_callback' => 'wp_filter_nohtml_kses',
				'type'	  => 'option','capability'		  => 'edit_theme_options',		));
	$wp_customize->add_control(
		new Sticky_Footswap_Number_Control(
			$wp_customize,
			'tdsfs[inner_narrow_mxw]',
			array(
				'label'	    => __( 'Where (px max width) is needed...', $author_plugin_slug ),
				'section'	=> 'tdsfs',
				'settings'	=> 'tdsfs[inner_narrow_mxw]',
				'priority'	=> 59
			)
		)
	);

	
	//narrow settings
	// Sticky Header change to smaller logo graphic + change menu to collapsed at specific width.

//EVERYTHINGS OK HERE
	// [disable_if_narrower] Sticky Header DISABLE if narrower than
	$wp_customize->add_setting(
		'tdsfs[disable_if_narrower]',
		array(
			'default'			=> '10',
			'sanitize_callback' => 'wp_filter_nohtml_kses',
			'type'				=> 'option',
			'capability'		=> 'edit_theme_options',
		)
	);
	$wp_customize->add_control(
		new Sticky_Footswap_Number_Control(
			$wp_customize,
			'tdsfs[disable_if_narrower]',
			array(
				'label'		=> __( 'DISABLE if narrower (px width)', $author_plugin_slug ),
				'section'	=> 'tdsfs',
				'settings'	=> 'tdsfs[disable_if_narrower]',
				'priority'	=> 60
			)
		)
	);



}  // END: function tdsfsfn_customize_register( $wp_customize )
// all we need with stickyfootswap is choose pages


/**
 * Returns plugin settings.
 *
 * @return    array    Merged array of plugin settings and plugin defaults.
 */

function tdsfsfn_get_settings() {
	$plugin_defaults = array(
/*		'mcheck_pages'				=> '',
		'mcheck_posts'				=> '',
		'mcheck_cats'				=> '',*/
		'show_at'				=> '100',
		'background_color'		=> '#ffffff',
		'text_color'			=> '#121212',
		'inner_max_width'			=> '1200',
		'inner_medium_mxw_bpoint'	=> '1200',
		'inner_medium_mxw'			=> '980',
		'inner_narrow_mxw_bpoint'	=> '980',
		'inner_narrow_mxw'			=> '770',
		'disable_if_narrower'	=> '10',

	);
	$plugin_settings = get_option( 'tdsfs' );



	//DEBUG LINE
	debug_to_console( $plugin_settings );

	return wp_parse_args( $plugin_settings, $plugin_defaults ); // Merge user defined arguments into defaults array
}