<?php
/**
* Represents the view for the public-facing component of the plugin.
*
* This typically includes any information, if any, that is rendered to the
* frontend of the theme when the plugin is activated.
 *
 * it's currently controlling:
 * 1. wether a logo image or blogname appears in $tdsfs_left_foot
 * 2. wether a menu appears or not
*/
// Get Sticky Footswap options
$tdsfs_pub_settings = get_option( 'tdsfs' );
// Check if there is a logo image
// esc_attr() - Filter a string cleaned and escaped for output in an HTML attribute stripped of invalid or special characters before output
if ( '' != $tdsfs_pub_settings['wide_state_logo'] ) :
	$tdsfs_left_foot = '<img src="' . esc_attr( $tdsfs_pub_settings['wide_state_logo'] ) . '" alt="' . esc_attr( get_bloginfo( 'description' ) ) . '" />';
else :
	$tdsfs_left_foot = get_bloginfo( 'name' );
endif;
if ( '' != $tdsfs_pub_settings['narrow_state_expandicon'] ) :
	$tdsfs_expandicon = '<img src="' . esc_attr( $tdsfs_pub_settings['narrow_state_expandicon'] ) . '" />';
else :
	$tdsfs_expandicon = get_bloginfo( 'name' );
endif;
?>
<style>

</style>

<div id="stickyfootswap">
	<div id="stickyfootswap-inner">

		<!--(FOOTER LEFT)-->
		<div id="stickyfootswap-logo">
			<a href="<?php echo bloginfo( 'url' ); ?>" title="<?php bloginfo( 'description' ); ?>">
				<?php echo $tdsfs_left_foot; ?> <!--display logo image wrapped in a detailed a element-->
			</a>
		</div>
		<!--(+) IF greater than threshold  -->

		<!--(+) AND IF 'menu' isset  -->
		<?php /*if( isset($tdsfs_pub_settings['menu']) ) :
			$menu_args = array(
				'menu'			=> $tdsfs_pub_settings['menu'],
				'depth'			=> 1,
				'menu_id'		=> 'stickyfootswap-menu',
				'container'		=> '',
				'fallback_cb'	=> ''
			);
			wp_nav_menu( $menu_args );
		endif; */?>
<!--
	$this_url = wp_url();
	$footswap_choices_array = get_option('tdsfs');
	$this_link = get_permalink();  //left empty in the loop it gets current post ID

	for (i=0; i<=$footswap_choices_array.length; i++){
		if ($this_url == $footswap_choices_array[i]){
		<HIDE FOOTER, DISPLAY STICKY FOOTER>
		}
	}
    -->

		<!--(FOOTER RIGHT)-->
		<div id="stickyfootswap-widget">
		<?php if ( '' != $tdsfs_pub_settings['selected_widget'] ) : ?> <!-- IF we selected a widget, use the widget here, ELSE leave blank-->
			<!--display the image, doesnt need 'a' element-->
			<?php echo $tdsfs_expandicon; ?>
		<?php else : ?>
		<?php endif; ?>

		</div>
	</div><!-- #stickyfootswap-inner -->
</div><!-- #stickyfootswap -->

<!-- IF span expandicon is selected, #stickyfootswap-widget span{display: block; }
 	IF an image is chosen, set #stickyfootswap-widget span{display: none; }
 		-and insert the image into #stickyfootswap-widget


 		Give options to control:
 		-Logo size and position
 		-
 		-
 		-
 		-
 	-->