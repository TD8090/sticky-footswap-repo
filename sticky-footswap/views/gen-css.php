<?php
/**
* Generated CSS
*
* This typically includes any information, if any, that is rendered to the
* frontend of the theme when the plugin is activated.
*/
// Get Sticky Footswap options

/**	 *  generated CSS (set up a css file to load in the wp_head)	 */
$plugin_settings = tdsfsfn_get_settings();
$tdsfs_bgimage = 'url(' . esc_attr($plugin_settings['background_image']) . ') repeat-x'
?>
<style type="text/css">
	#stickyfootswap {
	/*IF bg image set*/


		background-color: <?php echo $plugin_settings['background_color']; ?>;

	<?php if ( '' != $plugin_settings['background_image'] ) : ?>
		background-image: <?php echo esc_attr($plugin_settings['background_image']); ?>;

	<?php endif; ?>
	}

	.is-sticky {
		/*background-color: rgba(255, 255, 255, 0.93) !important;*/
		opacity: 1 !important;
		bottom: 0 !important;
	}
	#stickyfootswap,
	#stickyfootswap a {
		color: <?php echo $plugin_settings['text_color']; ?> !important;
	}


	/*FOCUS YOUR INNER WIDTH CHAKRAS*/
		/* we can set cascading inner-widths
		to better mimic any responsive theme    */
	<?php if ( isset( $plugin_settings['inner_max_width'] ) ) : ?>
	<?php if ( '' != $plugin_settings['inner_max_width'] ) : ?>
	#stickyfootswap-inner {
		max-width: <?php echo $plugin_settings['inner_max_width']; ?>px;
		margin: 0 auto;
	}
	<?php endif; ?>
	<?php endif; ?>
	<?php if ( isset( $plugin_settings['inner_medium_mxw'] ) ) : ?>
	<?php if ( '' != $plugin_settings['inner_medium_mxw'] ) : ?>
	@media only screen and (max-width: <?php echo $plugin_settings['inner_medium_mxw_bpoint']; ?>px) {
		#stickyfootswap-inner {
		max-width: <?php echo $plugin_settings['inner_medium_mxw']; ?>px;
	}
	<?php endif; ?>
	<?php endif; ?>
	<?php if ( isset( $plugin_settings['inner_narrow_mxw'] ) ) : ?>
	<?php if ( '' != $plugin_settings['inner_narrow_mxw'] ) : ?>
	@media only screen and (max-width: <?php echo $plugin_settings['inner_narrow_mxw_bpoint']; ?>px) {
		#stickyfootswap-inner {
		max-width: <?php echo $plugin_settings['inner_narrow_mxw']; ?>px;
	}
	<?php endif; ?>
	<?php endif; ?>
	/*FINISHED FOCUSING INNER WIDTH CHAKRAS*/
	@media only screen and (max-width: <?php echo $plugin_settings['wn_threshold']; ?>px) {
		.stickyfootswap {
			padding-top: 5px !important;
		}
		.logo {
			float: none;
		}
		#stickyfootswap-menu {
			padding-top: 10px;
			/*margin-bottom: 22px;*/
			float: left;
			text-align: center;
			width: 100%;
		}
		#stickyfootswap-menu li {
			width: 100%;
			padding: 7px 0;
			margin: 0;
		}
		#stickyfootswap-widget {
			display: block;
		}
	}
	/* */



</style>
